#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv)
{
	if (argc == 2)
	{
		if (!strcmp(argv[1], "ok"))
		{
			printf("%s", "it's working!\n");
			return 0;
		}
	}
	printf("%s", "Error!\n");
	return 1;
}